class KeyboardService {
  constructor() {
    this._keysPressed = new Set(); // codes of currently pressed keys
    this._handlers = new Map(); // K - code; V - list of callbacks for K pressed

    window.addEventListener('focus', () => {
      this._keysPressed.clear();
    });

    window.addEventListener('keydown', ({ code }) => {
      if (!this.isBeingPressed(code)) {
        this._handlers.get(code)?.forEach(handler => handler());
      }
      this._keysPressed.add(code);
    });

    window.addEventListener('keyup', ({ code }) => {
      this._keysPressed.delete(code);
    });
  }

  isBeingPressed(code) {
    return this._keysPressed.has(code);
  }

  areBeingPressed(codes) {
    return codes.every(code => this._keysPressed.has(code));
  }

  /** 
   * The handler fires once the key is pressed.
   * It **won't be firing rapidly** while holding the key.
   */
  addKeyHandler(code, callback) {
    this._handlers?.get(code)?.push(callback) 
      ?? this._handlers.set(code, [callback]);
  }

  /** 
   * The handler fires once all the keys are pressed simultaneously.
   * It **won't be firing rapidly** while holding the keys.
   */
  addMultipleKeysHandler(codes, callback) {
    const handleSingleCode = code => {
      const restKeys = codes.filter(c => c !== code); 
      if (this.areBeingPressed(restKeys)) {
        callback();
      }
    };

    codes.forEach(code => this.addKeyHandler(code, () => handleSingleCode(code)));
  }
}

export const keyboardService = new KeyboardService();