class ConsoleReporter {
  constructor() {
    this._reportingEnabled = false;
  }

  get reportingEnabled() {
    return this._reportingEnabled;
  }

  set reportingEnabled(isEnabled) {
    this._reportingEnabled = isEnabled;
  }

  report(message) {
    if (this.reportingEnabled) {
      console.log(message);
    }
  }

  reportFightStarted() {
    this.report('Fight!');
  }

  reportPlayerHit(attackerName, defenderName, damage) {
    this.report(`${attackerName} hits ${defenderName} with ${damage.toFixed(2)} of power.`);
  }

  reportPlayerBlocked(defenderName) {
    this.report(`${defenderName} blocked the attack!`);
  }

  reportPlayerWon(winnerName) {
    this.report(`${winnerName} has won the fight!`);
  }
}

export const consoleReporter = new ConsoleReporter();