export function clampToNonNegative(number) {
  return Math.max(0, number);
}