import { createElement } from '../helpers/domHelper';

export function createFighterPreview(fighter, position) {
  const positionClassName = position === 'right' ? 'fighter-preview___right' : 'fighter-preview___left';
  const fighterElement = createElement({
    tagName: 'div',
    className: `fighter-preview___root ${positionClassName}`,
  });

  if (fighter !== undefined) {
    fighterElement.append(
      createFighterImage(fighter),
      createFighterParameters(fighter)
    );
  }

  return fighterElement;
}

export function createFighterImage(fighter) {
  const { source, name } = fighter;
  const attributes = { 
    src: source, 
    title: name,
    alt: name 
  };
  const imgElement = createElement({
    tagName: 'img',
    className: 'fighter-preview___img',
    attributes,
  });

  return imgElement;
}

function createFighterParameters(fighter) {
  const list = createElement({ tagName: 'div', className: 'fighter-preview___parameters-list' });

  list.append(
    createFighterParametersItem('Name', fighter.name),
    createFighterParametersItem('Attack', fighter.attack),
    createFighterParametersItem('Defense', fighter.defense),
    createFighterParametersItem('Health', fighter.health)
  );

  return list;
}

function createFighterParametersItem(parameterName, parameterValue) {
  const element = createElement({ tagName: 'p', className: 'fighter-preview___parameters-item' });
  element.textContent = `${parameterName}: ${parameterValue}`;
  return element;
}