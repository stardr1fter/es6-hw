import { controls } from '../../constants/controls';
import { clampToNonNegative } from '../helpers/mathHelper';
import { fightParameters } from '../../constants/fightParameters';
import { keyboardService as keyboard } from '../services/keyboardService';
import { consoleReporter } from '../helpers/consoleReporter';

export async function fight(firstFighter, secondFighter, healthReporter = () => {}) {
  const playerOne = { // class extraction candidate
    name: 'Player One',
    fighter: firstFighter,
    health: firstFighter.health,
    criticalAttackReady: true,
    isBlocking: () => keyboard.isBeingPressed(controls.PlayerOneBlock)
  };

  const playerTwo = {
    name: 'Player Two',
    fighter: secondFighter,
    health: secondFighter.health,
    criticalAttackReady: true,
    isBlocking: () => keyboard.isBeingPressed(controls.PlayerTwoBlock)
  };

  consoleReporter.reportingEnabled = fightParameters.ReportToConsole;
  consoleReporter.reportFightStarted();

  return new Promise((resolve) => {
    const handleDamageReceived = (playerAttacking, playerDefending, damage) => {
      consoleReporter.reportPlayerHit(playerAttacking.name, playerDefending.name, damage);
      playerDefending.health = playerDefending.health - damage;
      let healthPercentage = playerDefending.health / playerDefending.fighter.health;

      if (playerDefending.health <= 0) {
        healthPercentage = 0;
        consoleReporter.reportPlayerWon(playerAttacking.name);
        resolve(playerAttacking.fighter);
      }

      healthReporter(playerDefending === playerOne ? 'first' : 'second', healthPercentage);
    };

    const handleGeneralAttack = (playerAttacking, playerDefending) => {
      if (playerAttacking.isBlocking()) {
        return; // it is not allowed to attack while blocking
      }

      if (playerDefending.isBlocking()) {
        consoleReporter.reportPlayerBlocked(playerDefending.name);
        return; // general attacks can be blocked entirely
      }

      const damage = getDamage(playerAttacking.fighter, playerDefending.fighter);
      handleDamageReceived(playerAttacking, playerDefending, damage);
    };

    const handleCriticalAttack = (playerAttacking, playerDefending) => {
      if (playerAttacking.isBlocking()) {
        return; // it is not allowed to attack while blocking
      }

      if (!playerAttacking.criticalAttackReady) {
        return; // there is a cooldown for the critical attack
      }

      const damage = getCriticalAttackDamage(playerAttacking.fighter);
      handleDamageReceived(playerAttacking, playerDefending, damage);

      playerAttacking.criticalAttackReady = false;
      setTimeout(() => playerAttacking.criticalAttackReady = true, fightParameters.CriticalAttackCooldown);
    };

    keyboard.addKeyHandler(controls.PlayerOneAttack, () => {
      handleGeneralAttack(playerOne, playerTwo);
    });

    keyboard.addKeyHandler(controls.PlayerTwoAttack, () => {
      handleGeneralAttack(playerTwo, playerOne);
    });

    keyboard.addMultipleKeysHandler(controls.PlayerOneCriticalHitCombination, () => {
      handleCriticalAttack(playerOne, playerTwo);
    });

    keyboard.addMultipleKeysHandler(controls.PlayerTwoCriticalHitCombination, () => {
      handleCriticalAttack(playerTwo, playerOne);
    });
  });
}

export function getDamage(attacker, defender) {
  const initialDamage = getHitPower(attacker);
  return reduceDamage(initialDamage, defender);
}

export function getHitPower(fighter) {
  return fighter.attack * randomizeCriticalHitChance();
}

export function getBlockPower(fighter) {
  return fighter.defense * randomizeDodgeChance();
}

function getCriticalAttackDamage(attacker) {
  return attacker.attack * 2; // damage is not reduced for critical attack
}

// Defender blocks random part of the damage
function reduceDamage(damage, defender) {
  return clampToNonNegative(damage - getBlockPower(defender));
}

function randomizeCriticalHitChance() {
  return Math.random() + 1;
}

function randomizeDodgeChance() {
  return Math.random() + 1;
}

