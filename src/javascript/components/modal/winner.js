import { showModal } from './modal';
import { createElement } from '../../helpers/domHelper';

export function showWinnerModal(fighter) {
  const bodyElement = createElement('p');
  bodyElement.textContent = 'Good for him.';
  showModal({
    title: `${fighter.name} wins!`,
    bodyElement: bodyElement,
    onClose: () => window.location.reload()
  });
}
