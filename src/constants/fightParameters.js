export const fightParameters = {
  // assuming all time units are in milliseconds
  CriticalAttackCooldown: 10_000,
  ReportToConsole: true
}